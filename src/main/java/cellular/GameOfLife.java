package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

/**
 * 
 * A CellAutomata that implements Conways game of life.
 * 
 * @see CellAutomaton
 * 
 *      Every cell has two states: Alive or Dead. Each step the state of each
 *      cell is decided from its neighbors (diagonal, horizontal and lateral).
 *      If the cell has less than two alive Neighbors or more than three
 *      neighbors the cell dies. If a dead cell has exactly three neighbors it
 *      will become alive.
 * 
 * @author eivind
 * @author Martin Vatshelle - martin.vatshelle@uib.no
 * @author Sondre Bolland - sondre.bolland@uib.no
 */
public class GameOfLife implements CellAutomaton {

	/**
	 * The grid of cells
	 */
	IGrid currentGeneration;

	/**
	 * 
	 * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */

	int height;
	int width;
	
	public GameOfLife(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();

		this.height = rows;
		this.width = columns;
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		return this.currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		return this.currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		return this.currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		CellState newState;
		IGrid nextGeneration = currentGeneration.copy();
		/* Rows: */
		for (int i=0; i < this.height; i++) {
			/* Columns:*/
			for (int j=0; j <this.width; j++) {

				newState = getCellState(i, j);
				nextGeneration.set(i, j, newState);

			}
		}
		this.currentGeneration = nextGeneration.copy();

	}

	@Override
	public CellState getNextCell(int row, int col) {
		int neighbors = this.countNeighbors(row, col, CellState.ALIVE);
		CellState state = this.getCellState(row, col);
		CellState newState = this.nextGenState(state, neighbors);
		return newState;
	}

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	private int countNeighbors(int row, int col, CellState state) {
		
		// Ints for the coordinates of the neighbour-cells:
		int nRow;
		int nCol;

		// Count the cells of the given state:
		int count = 0;
		CellState neighbourState; 

		// Loop through the neighbour-cells:
		/* i for rows: */
		for (int i=-1; i<=1; i++) {
			/* j for columns: */
			for (int j=-1; j <=1; j++) {

				// The neighbour-coordinates:
				nRow = row + i;
				nCol = col + j;
				// If the coordinates are for a cell inside the grid, that is not the same cell:
				if ( isInsideGrid(row+i, col+j) && isNotSame(row, col, nRow, nCol) ) {
					neighbourState = this.getCellState(nRow, nCol);
					
					if (state.equals(neighbourState)) {
						count++;
					}
				}
			}
		}

		return count;
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}


	/* Helper-method to check if the input coordinates
	* are inside the grid. */
	private boolean isInsideGrid(int row, int col) {
		if ( (row < 0) && ( this.height <= row ) ) {return false;}
		else if ( (col < 0) && ( this.width <= row ) ) {return false;}
		else {return true;}
	}


	/* Helper-method to check if two coordinates are the same. */
	private boolean isNotSame(int row1, int col1, int row2, int col2) {
		if ((row1==row2) && (col1==col2)) {return false;}
		else {return true;}
	}

	/* This function returns the state of the cell in the next generation,
	* depending on its current state, and the number of alive neighbours. */
	private CellState nextGenState(CellState state, int neighbors) {
		// When cell is ALIVE:
		if (state == CellState.ALIVE) {
			if ((2 <= neighbors)&&(neighbors<=3)) {
				return CellState.ALIVE;
			}
			else {
				return CellState.DEAD;
			}

		}
						
		else {
			if (neighbors == 3) {
				return CellState.ALIVE;
			}
			else {
				return CellState.DEAD;
			}

		}

	}


}
