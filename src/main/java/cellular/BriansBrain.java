package cellular;

import java.util.Random;
import datastructure.IGrid;
import datastructure.CellGrid;

public class BriansBrain implements CellAutomaton {

    private int height;
    private int width;

    IGrid currentGeneration;

    public BriansBrain(int rows, int columns) {
        this.height = rows;
        this.width = columns;

        this.currentGeneration = new CellGrid(rows, columns);
        this.initializeCells();
    }

    @Override
    public CellState getCellState(int row, int column) {
        CellState state = this.currentGeneration.get(row, column);
        return state;
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
        // Rows i:
        for (int i=0; i < this.height; i++) {
            // Columns j:
            for (int j=0; j < this.width; j++) {
                this.currentGeneration.set( i, j, this.randomState(random) );
            }
        }
    }

    @Override
    public void step() {
		CellState newState;
		IGrid nextGeneration = currentGeneration.copy();
		/* Rows: */
		for (int i=0; i < this.height; i++) {
			/* Columns:*/
			for (int j=0; j <this.width; j++) {

				newState = getCellState(i, j);
				nextGeneration.set(i, j, newState);

			}
		}

		this.currentGeneration = nextGeneration.copy();
    }

    @Override
    public CellState getNextCell(int row, int col) {
        int nAlive = this.countNeighbors(row, col, CellState.ALIVE);
        CellState state = this.getCellState(row, col);
        CellState newState = this.nextGenState(state, nAlive);
        return newState;
    }

    @Override
    public int numberOfRows() {return this.height;}

    @Override
    public int numberOfColumns() {return this.width;}

    @Override
    public IGrid getGrid() {return currentGeneration;}


    private CellState randomState(Random random) {
        int randomInt = random.nextInt(3);
        if (randomInt == 1) {return CellState.ALIVE;}
        else if (randomInt == 2) {return CellState.DYING;}
        else {return CellState.DEAD;}
    }


    private int countNeighbors(int row, int col, CellState state) {
		
		// Ints for the coordinates of the neighbour-cells:
		int nRow;
		int nCol;

		// Count the cells of the given state:
		int count = 0;
		CellState neighbourState; 

		// Loop through the neighbour-cells:
		/* i for rows: */
		for (int i=-1; i<=1; i++) {
			/* j for columns: */
			for (int j=-1; j <=1; j++) {

				// The neighbour-coordinates:
				nRow = row + i;
				nCol = col + j;
				// If the coordinates are for a cell inside the grid, that is not the same cell:
				if ( isInsideGrid(row+i, col+j) && isNotSame(row, col, nRow, nCol) ) {
					neighbourState = this.getCellState(nRow, nCol);
					
					if (state.equals(neighbourState)) {
						count++;
					}
				}
			}
		}

		return count;
	}

	/* Helper-method to check if the input coordinates
	* are inside the grid. */
    private boolean isInsideGrid(int row, int col) {
		if ( (row < 0) && ( this.height <= row ) ) {return false;}
		else if ( (col < 0) && ( this.width <= row ) ) {return false;}
		else {return true;}
	}

    /* Helper-method to check if two coordinates are the same. */
	private boolean isNotSame(int row1, int col1, int row2, int col2) {
		if ((row1==row2) && (col1==col2)) {return false;}
		else {return true;}
	}

	/* This function returns the state of the cell in the next generation,
	* depending on its current state, and the number of alive neighbours. */
    private CellState nextGenState(CellState state, int neighbors) {
        if (state == CellState.ALIVE) {return CellState.DYING;}
        else if (state == CellState.DYING) {return CellState.DEAD;}
        else if (neighbors == 2){return CellState.ALIVE;}
        else {return CellState.DEAD;}
    }

}
