package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    // Number of rows and columns:
    int rows;
    int columns;
    // The grid of cell-states[row][column]:
    CellState[][] gridState;

    public CellGrid(int rows, int columns, CellState[][] initialState) {
		this.rows = rows;
        this.columns = columns;
        this.gridState = initialState;
	}

    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.columns = columns;
        this.gridState = new CellState[rows][columns];
            // For every row...
        for (int i=0; i < rows; i++){
                // For every element in that row...
            for (int j=0; j < columns; j++) {
                    // Set the element to the same value as initialState:
                this.gridState[i][j] = initialState;
            }
        }
    }

    public CellGrid(int rows, int columns) {
        this.rows = rows;
        this.columns = columns;
        this.gridState = new CellState[rows][columns];
            // For every row...
        for (int i=0; i < rows; i++){
                // For every element in that row...
            for (int j=0; j < columns; j++) {
                    // Set the element to the same value as initialState:
                this.gridState[i][j] = CellState.DEAD;
            }
        }
    }

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        this.gridState[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        CellState state = this.gridState[row][column];
        return state;
    }

    @Override
    public IGrid copy() {
        CellState[][] newGridState = new CellState[this.rows][this.columns];
        /* Go through every row: */
        for (int i = 0; i < this.rows; i++) {
            for (int j = 0; j < this.columns; j++) {
                newGridState[i][j] = this.gridState[i][j];
            }
        }

        return new CellGrid(this.rows, this.columns, newGridState);
    }
    
}
